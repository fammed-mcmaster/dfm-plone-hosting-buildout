What is this?
=============

Plone hosting with ZEO in one system account.

Features
--------

* Configurable number of Zope instances
* Initialization with parameters
* Easy to create separate DBs
* Automatically generated configuration for load-balancing using
  Apache HTTPD's ``mod_balance`` module
* More...

Pre-requisites
==============

Before starting you should make sure that the following development
libraries are installed in the system:

========================= ============================================
Feature                   Library
========================= ============================================
Building Python packages  python-dev

Fast XML/XSLT             libxml2, libxslt

JPEG support              libjpeg (6a or 6b)

                          http://www.ijg.org
                          http://www.ijg.org/files/jpegsrc.v6b.tar.gz
                          ftp://ftp.uu.net/graphics/jpeg/

PNG support               zlib (1.2.3 or later is recommended)

                          http://www.gzip.org/zlib/

OpenType/TrueType support freetype2 (2.3.9 or later is recommended)

                          http://www.freetype.org
                          http://freetype.sourceforge.net

Geolocation features      GEOS
========================= ============================================

Optionally you can install also:

==================== ============================================
Feature              Library
==================== ============================================
CMS support support  littleCMS (2.2 or later is recommended)

                     http://www.littlecms.com/

TIFF support         libtiff

                     http://libtiff.maptools.org

WebP support         WebP

                     http://webp.googlecode.com
==================== ============================================

If you have a recent Linux version, the libraries provided with the
operating system usually work just fine.  If some library is missing,
installing a prebuilt version (jpeg-devel, zlib-devel, etc) is usually
easier than building from source. For example, for Ubuntu 10.04
(lucid), you can install the following libraries:

.. code-block:: sh

   $ sudo apt-get install \
	python2.6-dev libxml2-dev libxslt1-dev \
	libjpeg-dev zlib1g-dev libfreetype6-dev libgeos-dev
   $ sudo apt-get install \
	liblcms2-dev libtiff4-dev

For Ubuntu WebP is only available from 11.10 (oneiric) and higher, and
can be installed with the command:

.. code-block:: sh

   $ sudo apt-get install libwebp-dev

Initialization
==============

The buildout bootstrap needs to be done using the ``init.cfg``, mainly
because there is not ``buildout.cfg`` yet:

.. code-block:: sh

   $ python2.6 bootstrap.py -c init.cfg

The next step is to generate local configuration file, ``local.cfg``:

.. code-block:: sh

   $ bin/buildout -c init.cfg install local_cfg

The local configuration file should be generated only once, running
the ``local_cfg`` part again will overwrite any local customizations
you might have added to ``local.cfg``.

Next the main configuration file is generated, ``buildout.cfg``.
Three profiles are supported at the moment:

development
    Same Zope/Plone add-ons as in production, plus some to facilitate
    troubleshooting, debugging, etc. Also, debugging is enabled by
    default.

testing
    Same configuration as production, but with recording of picked
    versions enabled.

production
    Only eggs needed for production, plus configuration for the final
    deployment.

To create a development buildout:

.. code-block:: sh

   $ bin/buildout -c init.cfg install development

In the initialization invocation of buildout options can be passed as
variable assignments in the ``options`` part, e.g.:
``options:foo=bar``.

The recognized variables are:

For ``local_cfg``
-----------------

admin_user
    Name of the user account created initially by Zope for
    administration.

pwd_length
    Length of admin password if greater than 32 (which is set to be
    the minimum length).

For the others
--------------

instances
    The number of instances to create. If not specified, the number of
    cores available in the system is taken.

base_port
    Number from where all the port numbers will be offset.

Examples
--------

The initial admin account will be named ``vork``:

.. code-block:: sh

   $ bin/buildout -c init.cfg options:admin_user=vork install local_cfg

Initializes both the local and main configuration files with a
production profile and only one instance:

.. code-block:: sh

   $ bin/buildout -c init.cfg options:instances=1 install local_cfg production

Building
========

After running the initialization phase, you will have a
``buildout.cfg`` so you can run buildout:

.. code-block:: sh

   $ bin/buildout

You will need to run this everytime you modify either ``local.cfg`` or
``buildout.cfg``, e.g.: to expand the buildout created in the last
example of the last section to have 4 instances:

.. code-block:: sh

   $ bin/buildout -c init.cfg options:instances=4 install production
   $ bin/buildout

Once you have the hosting configuration ready, it can be started with:

.. code-block:: sh

   $ bin/supervisord

More commands are available in the ``bin/`` directory.

.. important:: TODO list:

   * Document parts and things in ``bin/``.

Usage
=====

Themes from QuintaGroup
-----------------------

Themes bought from QuintaGroup are accessed through HTTP Basic Auth
using the credentials provided by QuintaGroup. To include them in the
buildout add the credentials to the ``.htaccess`` and the eggs to
``buildout.cfg``:

.. code-block:: ini

   [z2-instance]
   eggs +=
       quintagroup.theme.chaste == 1.3.5

Then run ``buildout`` to apply the change:

.. code-block:: sh

   $ bin/buildout

Load balancing with Apache
--------------------------

A configuration for a load balancer using Apache HTTPD's
``mod_balancer`` is automatically generated. It has been tested with
Apache HTTPD 2.4 only.

To use follow this steps:

#. Enable the ``mod_balance`` module

   .. code-block:: sh

      $ sudo a2enmod mod_balance
      $ sudo ln -s /path/to/buildout/etc/apache2/conf-available/plone-load-balancer.conf \
		   /etc/apache2/conf-enabled/
      $ sudo /etc/init.d/apache2 reload		# or the method most appropriate for your OS

#. Use the load balancer in your site configuration, e.g.:

   .. code-block:: apache

      ProxyVia On
      ProxyPass / balancer://plone4s/VirtualHostBase/http/site.tld:80/path/in/zope/VirtualHostRoot/
